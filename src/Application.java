import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Application {
    Deque deque;
    Scanner userInput = new Scanner(System.in);
    String FILE_NAME = "./src/data.txt";

    public void run() {
        System.out.println("Введите начальный массив значений.");
        this.deque = new Deque(userInput.nextLine().split(" "));
        showDequeState(this.deque);

        this.showMenu();
        while (true) {
            String command = inputCommand();
            handleCommand(command);
        }
    }

    private void handleCommand(String command) {
        switch(command) {
            case "-h": showMenu(); break;
            case "-s": showDequeState(this.deque); break;
            case "-r": readDataFromFile(this.deque); break;
            case "-pf": pushItemFront(this.deque); break;
            case "-pb": pushItemBack(this.deque); break;
            case "-p": popItemFront(this.deque); break;
            case "-f": showFirst(this.deque); break;
            case "-l": showLast(this.deque); break;
            case "-e": showIsEmpty(this.deque); break;
            case "-c": showSize(this.deque); break;
            case "-cl": clear(this.deque); break;
            case "-w": writeFile(this.deque); break;
            case "-q": exit();
            default: showUnknownCommand(); break;
        }
    }

    private void exit() {
        System.exit(0);
    }

    private String inputCommand() {
        System.out.print("Введите команду: ");
        return userInput.nextLine();
    }

    private void showUnknownCommand() {
        System.out.println("Неизвестная команда. Используйте -h для того, чтобы узнать список команд.");
    }

    private void showDequeState(Deque deque) {
        System.out.println("Состояние дека: " + deque.toString());
    }

    private void pushItemFront(Deque deque) {
        System.out.print("Введите значение: ");
        deque.pushFront(read());
    }

    private void pushItemBack(Deque deque) {
        System.out.print("Введите значение: ");
        deque.pushBack(read());
    }

    private void popItemFront(Deque deque) {
        System.out.println("Первый улалённый элемент: " + deque.popFront());
    }

    private void showFirst(Deque deque) {
        System.out.println("Первый элемент дека: " + deque.first());
    }

    private void showLast(Deque deque) {
        System.out.println("Последний элемент дека: " + deque.last());
    }

    private  void showIsEmpty(Deque deque) {
        System.out.println(deque.isEmpty() ? "Дек пуст." : "Дек не пуст.");
    }

    private void showSize(Deque deque) {
        System.out.println("Количество элементов в деке: " + deque.size());
    }

    private void clear(Deque deque) {
        deque.clear();
        System.out.println("Дек очищен!");
    }

    private void writeFile(Deque deque) {
        System.out.print("Введите имя файла: ");
        String fileName = read();
        File file = new File(fileName);
        BufferedWriter out = null;

        try {
            out = new BufferedWriter(new FileWriter(file));
            out.write(deque.toString());
            out .flush();
            out.close();
        } catch (IOException ex) {
            System.out.println("Что-то пошло не так.");
        }
    }

    private String read() {
        String input = userInput.next();
        userInput.nextLine();

        return input;
    }

    private void readDataFromFile(Deque deque) {
        System.out.print("Введите имя файла: ");
        String fileName = read();
        String[] data = this.readFile(fileName);

        int i = 0;
        int length = data.length;
        while (i < length) {
            deque.pushBack(data[i++]);
        }
    }

    private void showMenu() {
        System.out.println("-h - Вывод этого меню");
        System.out.println("-s - Вывести содержимое дека");
        System.out.println("-r - Считать содержимое дека из файла");
        System.out.println("-pf - Добавить элемент в начало");
        System.out.println("-pb - Добавить элемент в конец");
        System.out.println("-p - Удалить элемент с начала очереди и вывести его на экран");
        System.out.println("-f - Вывести элемент начала очереди");
        System.out.println("-l - Вывести эелемент конца очереди");
        System.out.println("-e - Проверка наличия элементов");
        System.out.println("-c - Узнать количество элементов в деке");
        System.out.println("-cl - Очистить дек");
        System.out.println("-w - Запись содержимого дека в файл");
        System.out.println("-q - Выход");
    }


    private String[] readFile(String fileName) {
        List<String> data = new ArrayList<String>();
        File file = new File(fileName);
        Scanner scanner = null;

        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException exception) {
            System.out.println("File " + fileName + " not found!");

            try {
                System.out.println("Данные будут считаны с файла: " + this.FILE_NAME);
                scanner = new Scanner(new File(this.FILE_NAME));
            } catch (FileNotFoundException innerException) {

            }
        }

        while (scanner != null && scanner.hasNextLine()) {
            String[] pieces = scanner.nextLine().split(" ");
            int i = 0;
            int length = pieces.length;

            while (i < length) {
                data.add(pieces[i++]);
            }
        }

        return data.toArray(new String[0]);
    }
}
