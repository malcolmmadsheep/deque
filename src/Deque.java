/**
 * Created by User on 02.04.2017.
 */

import java.util.Scanner;
import java.io.*;


public class Deque {
    private String[] items;
    private int N;
    private int _head;
    private int _tail;

    public Deque() {
        this.items = new String[1];
        this.N = 0;
        this._head = 0;
        this._tail = -1;
    }

    public Deque(String[] data) {
        this();

        int i = 0;
        int length = data.length;
        while (i < length) {
            this.pushBack(data[i++]);
        }
    }

    public int size() {             //размер массива
        return this.N;
    }

    public boolean isEmpty() {      //проверяем на пустоту
        return this.N == 0;
    }

    public void pushFront(String s) {
        if (this.isFull()) {
            resize(this.items.length * 2);
        }
        for (int i = this.N; i > 0; i--) {
            this.items[i] = this.items[i - 1];
        }
        this.items[0] = s;
        this.N++;
    }

    public void pushBack(String s) {
        if (this.isFull()) {
            resize(this.items.length * 2);
        }
        this.items[this.N++] = s;
    }

    public boolean isFull() {
        return this.N == this.items.length;
    }

    public void clear() {
        this.items = new String[1];
        this.N = 0;
    }

    public String popFront() {
        if (this.isEmpty()) {
            return null;
        }

        String item = this.items[0];
        for (int i = 0; i < N - 1; i++) {
            this.items[i] = this.items[i + 1];
        }
        this.N--;
        if (this.N <= this.items.length / 4) {
            resize(this.items.length / 2);
        }

        return item;
    }

    public String first() {
        return isEmpty() ? null : this.items[0];
    }

    public String last() {
        return isEmpty() ? null : this.items[this.N - 1];
    }

    public String toString() {
        String s = "";

        for (int i = 0; i < N; i++) {
            s += this.items[i];

            if (i < N - 1) {
                s += " ";
            }
        }

        return s;
    }

    private void resize(int capacity)
    {
        if (capacity == 0) capacity = 4;

        String[] copy = new String[capacity];
        for (int i = 0; i < N; i++ ) {
            copy[i] = this.items[i];
        }

        this.items = copy;
    }
}
